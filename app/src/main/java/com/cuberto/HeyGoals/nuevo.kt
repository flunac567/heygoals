package com.cuberto.HeyGoals

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.otra_page.*
import kotlinx.android.synthetic.main.otra_page.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class nuevo : Fragment() {
    //View
    private lateinit var _view: View
    lateinit var mAuth: FirebaseAuth
    var mUser: FirebaseUser? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
       _view = inflater.inflate(R.layout.fragment_nuevo, container, false)

       volver()
        return _view
    }

    fun volver(){

        _view.btnVolver.setOnClickListener {
            AuthUI.getInstance().signOut(this.context!!).addOnSuccessListener{
                startActivity(Intent(this.context!!,Login::class.java))
                //Toast.makeText(this,"Vuelve pronto,te extrañaremos", Toast.LENGTH_SHORT).show()

                //finish()
            }.addOnFailureListener {
                //Toast.makeText(this,"Ocurrio un error ${it.message}", Toast.LENGTH_SHORT).show()
            }
        }
    }

}