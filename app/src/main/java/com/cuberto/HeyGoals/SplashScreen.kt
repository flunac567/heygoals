package com.cuberto.HeyGoals

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.google.firebase.auth.FirebaseAuth

class SplashScreen : AppCompatActivity() {

    //TODO el by lazy hace que solo inicialize esa variables cuando sea necesaria
    private val authUser: FirebaseAuth by lazy { FirebaseAuth.getInstance()}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        Handler().postDelayed({
            //Redirigir
            gotTo()
        }, 3000)


    }


    //Definir si va a la pantalla principal o a loguearse
    fun gotTo() {
        //Si el usuario esta logueado
        if (authUser.currentUser!=null){
            startActivity(Intent(this,Perfil::class.java))
            finish()
        }else{
            startActivity(Intent(this,Login::class.java))
            finish()
        }
    }
}